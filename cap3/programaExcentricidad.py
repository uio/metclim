#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  programaExcentricida.py
#  
#  Copyright 2022 Leonardo Tana <ltanav@tutanota.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import matplotlib.pyplot as plt
import numpy as np

from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)


def miexc1(A, tiempo, PenAnyos, phiEnGrados):	
	fexcentricidad = A * np.cos( 2*np.pi * ( ( tiempo/PenAnyos )+( phiEnGrados/360 ) ) )
	return fexcentricidad

# Constantes según la tabla
fA = np.array([ 0.010739, 0.008147, 0.006222, 0.005287, 0.004492 ])
fP = np.array([ 405091, 94932, 123945, 98857, 130781 ])
fphi = np.array([ 170.739, 109.891, -60.044, -86.140, 100.224 ])
dt = 4000
fe0 = 0.0275579
n=5

excentricidadtotal = np.zeros(shape=n)
total = np.zeros(shape=0)
mix = np.zeros(shape=0)

inicio = -1000000
fin = 1000000

print('Inicio programa')
for dt in range(inicio,fin+dt,dt):
	mix = np.append(mix, dt)
	for i in range(n):		
		excentricidadB = miexc1( fA[i], dt, fP[i], fphi[i])
		excentricidadtotal[i] = excentricidadB 
		#print('[',i,']',excentricidadtotal[i])
	total = np.append(total, np.sum(excentricidadtotal) + fe0)
#print(sumatorio[0])
print('--------')
print('excentricidad es ')
print("primer ele. {:e}".format(mix[0]))
print("ultimo elemento{:e}".format(mix[-1]))
print('--------')
plt.style.use('seaborn-darkgrid')
fig, ax = plt.subplots(figsize = (8,2))
plt.title('Ejemplo\n\n', fontweight ="bold")
ax.plot(mix, total)
ax.xaxis.set_minor_locator(AutoMinorLocator())
ax.yaxis.set_minor_locator(AutoMinorLocator())
plt.grid()
plt.show()
print('Fin programa')

